import os
import argparse


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    print('Filename \t\t\t Min-Cost')
    print('---------------------------------')
    files = args.files
    for file in files:
        f = os.popen('yoshiko -f ' + file + ' -v 1')
        file_lines = f.readlines()
        file_length = len(file_lines)
        last_line = file_lines[file_length - 1].split()
        line_length = len(last_line)
        cost = last_line[line_length - 1]
        file_name = file.split('/')[-1]
        print(file_name + '\t\t' + str(cost))


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('files', help='Files', nargs='+')
    return parser


if __name__ == "__main__":
    main()
