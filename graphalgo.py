import networkx as nx
import matplotlib.pyplot as plt
from operator import itemgetter

loop = 0
graph_edges_cost = set()
multi_edges = False


def start_algo(pos_graph, complete_graph, edge_adjustment=1, neighbor_adjustment=1, create_histogram=False, cost_range=0, first_gen=False):
    current_graph = nx.Graph(pos_graph.copy())
    total_cost = 0
    connected_components = nx.connected_component_subgraphs(current_graph)
    repaired_graphs = []
    for subgraph in connected_components:
        cost, clustered_subgraph = cluster_algo(
            subgraph, complete_graph,
            edge_adjustment, neighbor_adjustment, create_histogram, cost_range, first_gen)
        total_cost += cost
        repaired_graphs.append(clustered_subgraph)
        final_graph = nx.union_all(repaired_graphs)
    return total_cost, final_graph


def cluster_algo(pos_graph, complete_graph, edge_adjustment=1, neighbor_adjustment=1, create_histogram=False, cost_range=0, first_gen=False):
    '''Calculates clustering cost and clustered graphs for pos_graph and returns them'''
    global graph_edges_cost
    global multi_edges
    global loop
    total_cost = 0
    current_graph = nx.Graph(pos_graph.copy())
    closed_graph = nx.Graph(pos_graph.copy())
    cl_cost = closure_cost(closed_graph, complete_graph)
    if cl_cost == 0:
        return cl_cost, closed_graph
    if cost_range > 0:
        multi_edges = True
    while (current_graph.number_of_nodes() >= 2) and (total_cost < cl_cost):
        graph_edges_cost = edges_cost(
            current_graph, edge_adjustment, neighbor_adjustment)
        #print(graph_edges_cost)
        # Save histograms
        if create_histogram:
            loop += 1
            save_histogram()
        # Choose edge with highest weighting
        best_edge = max(graph_edges_cost, key=itemgetter(2))
        #print(best_edge)
        best_cost = best_edge[2]
        # If best_cost less or equal then threshold return closure_cost for
        # pos_graph
        if cl_cost < (total_cost + current_graph[best_edge[0]][best_edge[1]]['weight']):
            return cl_cost, closed_graph
        # Remove edges and repair new conflict-triples if asked for
        if not multi_edges:
            total_cost += current_graph[best_edge[0]][best_edge[1]]['weight']
            current_graph.remove_edge(best_edge[0], best_edge[1])
            if first_gen:
                total_cost += repair_new_conflict_triples(
                    current_graph, best_edge)
        else:
            if best_cost < 0:
                range_cost = best_cost * (1 + cost_range)
            else:
                range_cost = best_cost * (1 - cost_range)
            while graph_edges_cost:
                edge = graph_edges_cost.pop()
                if edge[2] >= range_cost:
                    total_cost += current_graph[edge[0]][edge[1]]['weight']
                    current_graph.remove_edge(edge[0], edge[1])
                    if first_gen:
                        total_cost += repair_new_conflict_triples(
                            current_graph, (edge[0], edge[1]))
        # If working_graph has been separated calculate new graphs
        if not nx.is_connected(current_graph):
            subgraphs = nx.connected_component_subgraphs(current_graph)
            repaired_graphs = []
            for graph in subgraphs:
                cost, graph_repaired = cluster_algo(
                    graph, complete_graph, edge_adjustment, neighbor_adjustment, create_histogram, cost_range, first_gen)
                total_cost += cost
                repaired_graphs.append(graph_repaired)
            if cl_cost is None or cl_cost > total_cost:
                final_graph = nx.union_all(repaired_graphs)
                return total_cost, final_graph
            else:
                return cl_cost, closed_graph
    # return total_cost, current_graph
    if cl_cost < total_cost:
        return cl_cost, closed_graph
    else:
        return total_cost, current_graph


def save_histogram():
    weights = []
    num_bins = 15
    for edge in graph_edges_cost:
        rounded_weight = round(edge[2])
        weights.append(rounded_weight)
    plt.figure()
    plt.hist(weights, num_bins, facecolor='blue', alpha=0.5)
    plt.xlabel('Weight')
    plt.ylabel('# Edges')
    plt.savefig("fig/figure" + str(loop) + ".png")
    plt.close()


def repair_new_conflict_triples(graph, removed_edge):
    '''
    Removes edges of conflict-triples created by removing 'edge' from 'graph' and returns overall cost
    '''
    cost = 0
    removed_node1 = removed_edge[0]
    removed_node2 = removed_edge[1]
    global graph_edges_cost
    joint_neighbors = list(
        set(graph.neighbors(removed_node1)) & set(graph.neighbors(removed_node2)))
    for neighbor in joint_neighbors:
        if graph[removed_node1][neighbor]['weight'] < graph[removed_node2][neighbor]['weight']:
            best_edge = (removed_node1, neighbor)
        elif graph[removed_node1][neighbor]['weight'] > graph[removed_node2][neighbor]['weight']:
            best_edge = (removed_node2, neighbor)
        else:
            best_edge = transitive_improvement(
                graph, (removed_node1, neighbor), (removed_node2, neighbor))
        cost += graph[best_edge[0]][best_edge[1]]['weight']
        graph.remove_edge(best_edge[0], best_edge[1])
        if multi_edges:
            for (node1, node2, weight) in graph_edges_cost:
                if (node1 == best_edge[0] and node2 == best_edge[1]) or (node2 == best_edge[0] and node1 == best_edge[1]):
                    graph_edges_cost.remove((node1, node2, weight))
                    break
    return cost


def closure_cost(working_graph, complete_graph):
    '''Return costs for transitive closure and transitive graph'''
    cost = 0
    reversed_graph = nx.complement(working_graph)
    for node1, node2 in reversed_graph.edges_iter():
        # if edge has been removed before decrease cost
        if complete_graph[node1][node2]['weight'] > 0:
            cost -= complete_graph[node1][node2]['weight']
        # if edge is negative edge
        else:
            cost += abs(complete_graph[node1][node2]['weight'])
        working_graph.add_edge(
            node1, node2, weight=complete_graph[node1][node2]['weight'])
    return cost


def transitive_improvement(graph, edge1, edge2):
    '''Returns edge with best transitive_improvement by counting number of remaining conflict-edges after removing edge1 or edge2'''
    graph1 = nx.Graph(graph.copy())
    graph2 = nx.Graph(graph.copy())
    # Count number of conflict-edges in graph after removing edge1
    graph1.remove_edge(edge1[0], edge1[1])
    graph1_conflict_edges = len(conflict_edges(graph1, 1, 1))
    # Count number of conflict-edges in graph after removing edge2
    graph2.remove_edge(edge2[0], edge2[1])
    graph2_conflict_edges = len(conflict_edges(graph2, 1, 1))
    if graph1_conflict_edges < graph2_conflict_edges:
        return edge1
    else:
        return edge2


def edges_cost(graph, edge_adjustment, neighbor_adjustment):
    '''Returns a list of all edges and their estimated removal cost'''
    edges = set()
    for node1, node2 in graph.edges_iter():
        weight = -edge_adjustment * graph[node1][node2]['weight']
        weight += neighbor_edges_weighting(
            graph, node1, node2, neighbor_adjustment)
        weight += neighbor_edges_weighting(
            graph, node2, node1, neighbor_adjustment)
        edges.add((node1, node2, weight))
    return edges


def conflict_edges(graph, edge_adjustment, neighbor_adjustment):
    '''Returns a list of all edges of conflict triples and their weighting-costs'''
    conflict_edges = []
    for node1, node2 in graph.edges_iter():
        disjunct_neighbors_node1 = list(
            (set(graph.neighbors(node1)) - set([node2])) - set(graph.neighbors(node2)))
        disjunct_neighbors_node2 = list(
            (set(graph.neighbors(node2)) - set([node1])) - set(graph.neighbors(node1)))
        if disjunct_neighbors_node1 or disjunct_neighbors_node2:
            weight = -edge_adjustment * graph[node1][node2]['weight']
            weight += neighbor_edges_weighting(
                graph, node1, node2, neighbor_adjustment)
            weight += neighbor_edges_weighting(
                graph, node2, node1, neighbor_adjustment)
            conflict_edges.append((node1, node2, weight))
    return conflict_edges


def neighbor_edges_weighting(graph, node1, node2, neighbor_adjustment):
    '''
    Determines the overall weighting for all neighbor-edges of node1 except edge (node1, node2)
    '''
    weight = 0
    for neighbor in graph.neighbors(node1):
        if neighbor != node2:
            if neighbor in graph.neighbors(node2):
                weight -= neighbor_adjustment * graph[node1][neighbor]['weight']
            else:
                weight += graph[node1][neighbor]['weight']
    return weight
