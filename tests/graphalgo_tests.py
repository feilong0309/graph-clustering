from graphalgo import neighbor_edges_weighting, conflict_edges, transitive_improvement, closure_cost, repair_new_conflict_triples
import networkx as nx

def test_neighbor_edges_weighting():
	#No Neighbors
	graph = nx.Graph()
	graph.add_edge(1,2, weight = 1)
	assert neighbor_edges_weighting(graph, 1, 2, 1) == 0
	#Joint-Neighbors
	graph.add_edge(1, 3, weight = 2)
	graph.add_edge(2, 3, weight = 4)
	assert neighbor_edges_weighting(graph, 1, 2, 1) == -2
	assert neighbor_edges_weighting(graph, 2, 1, 1) == -4
	#Disjoint-Neighbors
	graph1 = nx.Graph()
	graph1.add_edge(1,2, weight = 1)
	graph1.add_edge(2,3, weight = 1)
	assert neighbor_edges_weighting(graph1, 1, 2, 1) == 0
	assert neighbor_edges_weighting(graph1, 2, 1, 1) == 1
	graph1.add_edge(1,4, weight = 5)
	assert neighbor_edges_weighting(graph1, 1, 2, 1) == 5
	assert neighbor_edges_weighting(graph1, 2, 1, 1) == 1
	graph1.add_edge(1,6, weight = 6)
	graph1.add_edge(3,6, weight = 8)
	assert neighbor_edges_weighting(graph1, 1, 2, 1) == 11
	assert neighbor_edges_weighting(graph1, 2, 1, 1) == 1
	#Joint and Disjoint-Neighbors
	graph1.add_edge(1, 5, weight = 4)
	graph1.add_edge(2, 5, weight = 6)
	assert neighbor_edges_weighting(graph1, 1, 2, 1) == 7
	assert neighbor_edges_weighting(graph1, 2, 1, 1) == -5

def test_conflict_edges():
	graph = nx.Graph()
	#Simple Graph without conflict-edges
	graph.add_edge(1,2, weight = 3)
	assert conflict_edges(graph, 1, 1) == []
	#non-transitive graph with 3 vertices, no junction
	graph.add_edge(1, 3, weight = 4)
	assert conflict_edges(graph, 1, 1) == [(1, 2, 1), (1,3, -1)]
	#transitive graph with 3 vertices
	graph.add_edge(2, 3, weight = 5)
	assert conflict_edges(graph, 1, 1) == []
	#graph with 4 vertices and 3 non-transitive conflict_edges
	graph.add_edge(2, 4, weight = 5)
	assert conflict_edges(graph, 1, 1) == [(1, 2, -7),(2, 3, -7),(2, 4, 3)]

def test_transitive_improvement():
	graph = nx.Graph()
	graph.add_edge(1,2, weight = 1)
	graph.add_edge(1,3, weight = 1)
	graph.add_edge(2,3, weight = 1)
	assert transitive_improvement(graph, (1,2), (1,3)) == (1,3)
	graph.add_edge(2,4, weight = 1)
	graph.add_edge(3,4, weight = 1)
	assert transitive_improvement(graph, (1,2), (2,3)) == (1,2)
	graph1 = nx.Graph()
	graph1.add_edge(1,2, weight = 1)
	graph1.add_edge(1,3, weight = 1)
	graph1.add_edge(1,4, weight = 1)
	assert transitive_improvement(graph1, (1,2), (1,4)) == (1,4)
	assert transitive_improvement(graph1, (1,2), (1,3)) == (1,3)

def test_closure_cost():
	#Close conflict-triple
	complete_graph = nx.Graph()
	complete_graph.add_edge(1, 2, weight = 5)
	complete_graph.add_edge(1, 3, weight = 20)
	complete_graph.add_edge(2, 3, weight = -4)
	positive_graph = nx.Graph()
	positive_graph.add_edge(1, 2, weight = 5)
	positive_graph.add_edge(1, 3, weight = 20)
	assert closure_cost(positive_graph, complete_graph) == 4
	#transitive triple
	positive_graph = nx.Graph()
	positive_graph.add_edge(1, 2, weight = 5)
	positive_graph.add_edge(1, 3, weight = 20)
	positive_graph.add_edge(2, 3, weight = 4)
	assert closure_cost(positive_graph, complete_graph) == 0
	#one edge removed before
	complete_graph = nx.Graph()
	complete_graph.add_edge(1, 2, weight = 3)
	complete_graph.add_edge(1, 3, weight = 2)
	complete_graph.add_edge(2, 3, weight = 4)
	positive_graph = nx.Graph()
	positive_graph.add_edge(1, 2, weight = 3)
	positive_graph.add_edge(1, 3, weight = 2)
	assert closure_cost(positive_graph, complete_graph) == -4

def test_repair_new_conflict_triples():
	#conflict-triple
	graph = nx.Graph()
	graph.add_edge(1, 2, weight = 4)
	graph.add_edge(1, 3, weight = 3)
	assert repair_new_conflict_triples(graph, (2, 3)) == 3
	#transitive-triple
	graph = nx.Graph()
	graph.add_edge(1, 2, weight = 4)
	graph.add_edge(1, 3, weight = 3)
	graph.add_edge(2, 4, weight = 3)
	graph.remove_edge(2, 4)
	graph.add_edge(2, 3, weight = 5)
	assert repair_new_conflict_triples(graph, (2, 4)) == 0
	#4 vertices with 4 conflict-edges
	graph = nx.Graph()
	graph.add_edge(1, 2, weight = 4)
	graph.add_edge(1, 3, weight = 3)
	graph.add_edge(2, 4, weight = 6)
	graph.add_edge(3, 4, weight = 7)
	assert repair_new_conflict_triples(graph, (2, 3)) == 9
	#3 concatenated conflict-edges
	graph = nx.Graph()
	graph.add_edge(1, 2, weight = 3)
	graph.add_edge(2, 3, weight = 3)
	graph.remove_edge(2, 3)
	graph.add_edge(3, 4, weight = 3)
	assert repair_new_conflict_triples(graph, (2, 3)) == 0