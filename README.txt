Relevanter Code:
	graphclustering.py
	graphread.py
	graphalgo.py

Benötigte Bibliotheken:
	NetworkX: http://networkx.github.io
	Numpy: http://www.numpy.org

Entwickelt unter Python 3.3.4

Programmausführung:

usage: graphclustering.py [-h] [-g] [-t] [-hg] [-i] [-fg]
                          [-mer MULTI_EDGE_REMOVAL] [-ac ADJUST_COST] [-c]
                          [-th THRESHOLD]
                          filepath [filepath ...]

positional arguments:
  filepath              Component Matrix File containing graph information

optional arguments:
  -h, --help            show this help message and exit
  -g, --graphs          Draws origin and clustered graphs
  -t, --tgf             Trivial Graph Format instead CM-Format
  -hg, --histogram      Prints a histogram containing number of edge-weights
                        assigned by targetfunction
  -i, --intense         Runs algorithm ten times with random edge and neighbor
                        adjustment in range [0.9;1.1]. Shows best cost.
  -fg, --first_generation
                        Removes conflict-edges created by last removed edge
  -mer MULTI_EDGE_REMOVAL, --multi_edge_removal MULTI_EDGE_REMOVAL
                        Removes all edges in each step, which are in the
                        specified range of edge with best cost
  -ac ADJUST_COST, --adjust_cost ADJUST_COST
                        Manipulates cost of the initial Graph by adding a
                        constant costvalue
  -c, --combined        Runs algorithm in with and without first_gen mode and
                        takes best cost

Zusätzlicher Code (Achtung: statisch programmiert und ggf. nicht lauffähig):
	yoshiko_executer.py
	result_comparison.py