import networkx as nx
import sys


class ParseError(Exception):
    pass


def read_tgf(path, cost_adjustment):
    file = open(path, "r")
    nodes = {}
    edges = []
    i = 1
    for line in file:
        edge = line.split()
        if edge[0] not in nodes:
            nodes[edge[0]] = i
            i += 1
        if edge[1] not in nodes:
            nodes[edge[1]] = i
            i += 1
        edges.append(
            (nodes[edge[0]], nodes[edge[1]], float(edge[2]) + cost_adjustment))
    graph = create_graph(edges)
    return graph


def read_cm(path, cost_adjustment):
    """
    Read 'component matrix' file.
    File format is:
    - First line is an integer n: the number of vertices
    - Then a list of n vertex names, one per line
    - Then n-1 lines. first has n-1 tab-separated fields with edge weights,
    second has n-2, and so on. There will be only one entry in the last
    line.

    In this version the vertex names are ignored.

    Returns a Graph.
    """

    file = open(path, "r")
    n = int(file.readline().rstrip())

    # skip vertices names
    for i in range(n):
        file.readline()

    # save edges with weights as a list
    edges = []
    for i in range(n - 1):
        line = file.readline()
        try:
            if not line.endswith('\n'):
                raise ParseError
        except ParseError:
            print("Error: file ended prematurely")
            sys.exit(1)
        weights = line[:-1].split('\t')
        try:
            if '0' in weights:
                raise ValueError
            if len(weights) != n - 1 - i:
                raise ParseError
        except ValueError:
            print('Error: Edge weights equal 0 are not allowed')
            sys.exit(1)
        except ParseError:
            print("expected %d weights, got %d" % (n - 1 - i, len(weights)))
            sys.exit(1)
        for j in range(n - 1 - i):
            edges.append(
                (i + 1, j + i + 2, float(weights[j]) + cost_adjustment))
    graph = create_graph(edges)
    return graph


def create_graph(edges):
    G = nx.Graph()
    for node1, node2, weight in edges:
        G.add_edge(node1, node2, weight=weight)
    return G


def remove_negative_edges(complete_graph):
    positive_graph = nx.Graph(complete_graph.copy())
    for node1, node2 in complete_graph.edges_iter():
        if complete_graph[node1][node2]['weight'] < 0:
            positive_graph.remove_edge(node1, node2)
    return positive_graph
