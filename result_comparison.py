import argparse
import sys
from numpy import log, exp


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    if args.improvement:
        file = args.filepath[0]
        determine_improvement(file)
    else:
        file1 = args.filepath[0]
        file2 = args.filepath[1]
        if args.yoshiko:
            results_yoshiko(file1, file2)
        else:
            result_comparison(file1, file2)


def results_yoshiko(heuristic_file, yoshiko_file):
    results_heuristic = open(heuristic_file, 'r')
    results_yoshiko = open(yoshiko_file, 'r')
    results_heuristic.readline()
    results_heuristic.readline()
    results_yoshiko.readline()
    total_cost_deviation = 0
    total_unqueal_costs = 0
    results_equal = 0
    lines = 0
    for line in results_heuristic:
        line_heuristic = line.split()
        line_yoshiko = results_yoshiko.readline().split()
        while line_yoshiko[4] == 'NA':
            line_yoshiko = results_yoshiko.readline().split()
        if line_yoshiko[4] != '0':
            lines += 1
            heuristic_cost = float(line_heuristic[1][:6])
            yoshiko_cost = float(line_yoshiko[4])
            print(str(heuristic_cost) + '\t' + str(yoshiko_cost))
            if heuristic_cost <= yoshiko_cost:
                results_equal += 1
            else:
                cost_mean_deviation = heuristic_cost / yoshiko_cost
                total_cost_deviation += log(cost_mean_deviation)
                total_unqueal_costs += 1
                print(line_heuristic[0] + ': ' + str(
                    heuristic_cost) + ' >= ' + str(yoshiko_cost))
    geometric_mean = exp(total_cost_deviation/total_unqueal_costs)
    print('Total Results: ' + str(lines))
    print('Equal Results: ' + str(results_equal))
    print('Percentage: ' + str(results_equal / lines * 100))
    print('Average Deviation: ' +
          str(geometric_mean))


def determine_improvement(file):
    result = open(file, 'r')
    lines = 0
    improvements = 0
    result.readline()
    result.readline()
    for line in result:
        lines += 1
        line_result = line.split()
        if line_result[3] == 'yes':
            improvements += 1
    improvement = improvements / lines * 100
    print('Number of improved costs: ' + str(improvements))
    print('Percentage:' + str(improvement))


def result_comparison(file1, file2):
    result1 = open(file1, 'r')
    result2 = open(file2, 'r')
    # Skip first two lines
    result1.readline()
    result1.readline()
    result2.readline()
    result2.readline()
    result1_better = 0
    result2_better = 0
    results_equal = 0
    lines = 0
    for line in result1:
        lines += 1
        line_result1 = line.split()
        line_result2 = result2.readline().split()
        if line_result1[0] == line_result2[0]:
            if float(line_result1[1]) < float(line_result2[1]):
                result1_better += 1
            elif float(line_result2[1]) < float(line_result1[1]):
                result2_better += 1
            else:
                results_equal += 1
        else:
            print('Wrong results compared!')
            sys.exit(0)
    result1_percent = result1_better / (lines / 100)
    result2_percent = result2_better / (lines / 100)
    equal_percent = results_equal / (lines / 100)
    result1_name = file1.split('/')
    result2_name = file2.split('/')
    print(result1_name[1] + " Best: " + str(
        result1_better) + "\t\t" + str(result1_percent) + "%")
    print(result2_name[1] + " Best: " + str(
        result2_better) + "\t\t" + str(result2_percent) + "%")
    print("Equal Cost: " + str(results_equal)
          + "\t\t" + str(equal_percent) + "%")


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('filepath', help='Two result files', nargs='+')
    parser.add_argument(
        '-i', '--improvement', help='Counts Improvment', action='store_true')
    parser.add_argument(
        '-y', '--yoshiko', help='Second File contains yoshiko results', action='store_true')
    return parser


if __name__ == "__main__":
    main()
