import argparse
import os.path
import random
from graphread import read_cm, read_tgf, remove_negative_edges
from graphalgo import start_algo
import matplotlib.pyplot as plt
import networkx as nx


def main():
    parser = ArgumentParser()
    args = parser.parse_args()
    first_gen = args.first_generation
    cost_range = args.multi_edge_removal
    cost_adjustment = args.adjust_cost
    combined = args.combined
    print("File-Name \t\t\t\t\t Clustering-Cost \t\t Stability \t Improved")
    print("----------------------------------------------------------------------------------------------------------")
    for fname in args.filepath:
        if args.tgf:
            complete_graph = read_tgf(fname, cost_adjustment)
        else:
            try:
                if os.path.splitext(fname)[1] != ".cm":
                    raise IOError
            except IOError:
                print('Error: Wrong File-Type! Choose cm-File.')
                break
            complete_graph = read_cm(fname, cost_adjustment)
        positive_graph = remove_negative_edges(complete_graph)
        save_histogram = args.histogram
        edge_adjustment = 1
        neighbor_adjustment = 1
        best_cost, best_clustered_graph = start_algo(
            positive_graph, complete_graph, edge_adjustment, neighbor_adjustment, save_histogram, cost_range, first_gen)
        if combined and not first_gen:
            first_gen_cost, first_gen_clustered_graph = start_algo(
                positive_graph, complete_graph, edge_adjustment, neighbor_adjustment, save_histogram, cost_range, True)
            if first_gen_cost < best_cost:
                best_cost = first_gen_cost
                best_clustered_graph = first_gen_clustered_graph
        if args.intense:
            standard_cost = best_cost
            stable = 'Stable\t'
            first_gen_cost, first_gen_clustered_graph = start_algo(
                positive_graph, complete_graph, edge_adjustment, neighbor_adjustment, save_histogram, cost_range, True)
            if first_gen_cost < best_cost:
                if stable == 'Stable\t':
                    stable = 'Unstable'
                best_cost = first_gen_cost
                best_clustered_graph = first_gen_clustered_graph
            for i in range(8):
                if i == 4:
                    first_gen = not first_gen
                edge_adjustment = random.uniform(0.9, 1.1)
                neighbor_adjustment = random.uniform(0.9, 1.1)
                cost, clustered_graph = start_algo(
                    positive_graph, complete_graph, edge_adjustment, neighbor_adjustment, save_histogram, cost_range, first_gen)
                if cost < best_cost:
                    if stable == 'Stable\t':
                        stable = 'Unstable'
                    best_cost = cost
                    best_clustered_graph = clustered_graph
            if best_cost < standard_cost:
                improved = 'yes'
            else:
                improved = 'no'
        else:
            stable = '--'
            improved = '--'

        print(str.split(fname, '/')[-1] + " \t\t" + str(
            best_cost) + " \t " + stable + "\t " + improved)
        # Save Graphs
        if args.graphs:
            # Origin Graph
            plt.figure(str.split(fname, '/')[-1] + " Origin Graph")
            nx.draw(positive_graph)
            plt.savefig(
                "fig/" + str.split(fname, '/')[-1] + " Origin Graph.png")
            # Clustered Graph
            plt.figure(str.split(fname, '/')[-1] + " Clustered Graph")
            nx.draw(best_clustered_graph)
            plt.savefig(
                "fig/" + str.split(fname, '/')[-1] + " Clustered Graph.png")


def ArgumentParser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'filepath', help='Component Matrix File containing graph information', nargs='+')
    parser.add_argument(
        '-g', '--graphs', help='Draws origin and clustered graphs', action='store_true')
    parser.add_argument(
        '-t', '--tgf', help='Trivial Graph Format instead CM-Format', action='store_true')
    parser.add_argument(
        '-hg', '--histogram', help='Prints a histogram containing number of edge-weights assigned by targetfunction', action='store_true')
    parser.add_argument(
        '-i', '--intense', help='Runs algorithm ten times with random edge and neighbor adjustment in range [0.9;1.1]. Shows best cost.', action='store_true')
    parser.add_argument('-fg', '--first_generation',
                        help='Removes conflict-edges created by last removed edge', action='store_true')
    parser.add_argument('-mer', '--multi_edge_removal',
                        help='Removes all edges in each step, which are in the specified range of edge with best cost', type=float, default=0)
    parser.add_argument(
        '-ac', '--adjust_cost', help='Manipulates cost of the initial Graph by adding a constant costvalue', type=int, default=0)
    parser.add_argument(
        '-c', '--combined', help='Runs algorithm with and without first_gen mode and takes best cost', action='store_true')
    return parser

if __name__ == "__main__":
    main()
